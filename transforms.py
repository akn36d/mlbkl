import numpy as np
import glob
import pickle 

from os.path import basename, join
from pprint import pprint
from torchvision import datasets, models, transforms
from PIL import Image

def sample_normalize(tensor):
    """Normalize a tensor image with mean and standard deviation.
    .. note::
        This transform acts in-place, i.e., it mutates the input tensor.
    See :class:`~torchvision.transforms.Normalize` for more details.
    Args:
        tensor (Tensor): Tensor image of size (C, H, W) to be normalized.
    Returns:
        Tensor: Normalized Tensor image.
    """
    if not _is_tensor_image(tensor):
        raise TypeError('tensor is not a torch image.')

    # This is faster than using broadcasting, don't change without benchmarking
    for t in zip(tensor):
        t.sub_(m).div_(s)
    return tensor