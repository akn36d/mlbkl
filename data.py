import numpy as np
import glob
import pickle 

from os.path import basename, join
from pprint import pprint
from sklearn.model_selection import StratifiedKFold, StratifiedShuffleSplit
from skimage import io
from torch.utils.data import Dataset, DataLoader 
from torchvision import datasets, models, transforms
from PIL import Image

from utils import recursive_glob, get_folder_above


def make_folds(data_dir, class_name_list, file_template, n_splits=10, save_folds=True, fold_prefix=None, save_dir=None):
    """Creates stratified splits based on train directory listing
    # Arguments
        data_dir : The data directory which has each class of the images in 
                   different subfolders 
        class_name_list : List of class names as strings
        file_template :  The image format 
        n_splits : Number of stratified splits desired (10 for 10-fold split)
        save_folds : Set to True if you want to save the folds created as a pickle file
        fold_prefix : The prefix for the fold file name
    # Dumps
        folds: list of splits dict{
                                   "train": {
                                               "x": train files list,
                                               "y": train labels},
                                   "test": {
                                               "x": test files list,
                                               "y": test labels}}
                                  }
    """
    files = recursive_glob(root_dir=data_dir, file_template=file_template)
#    pprint(files)
    file_labels = np.array([get_folder_above(f) for f in files])
   
    labels = []
    num_classes = len(class_name_list)

    classes = np.r_[0:num_classes]    
    for f in file_labels:
        lb = np.array([f.startswith(x) for x in class_name_list])
        labels.append(classes[lb])
    labels = np.array(labels)
    labels = labels.astype(np.uint8)
#    np.savetxt("labels2.csv", labels)

    # Creating file index list for k-fold cross validation
    folds = []
    files = np.asarray(files)
    skf = StratifiedKFold(n_splits=n_splits, shuffle=True)

#    print('STRATIFICATION INFO')
    for train_index, test_index in skf.split(files, labels):
#        print("TRAIN:", len(train_index), "TEST:", len(test_index))
        f_train, f_test = files[train_index], files[test_index]
        y_train, y_test = labels[train_index], labels[test_index]
        folds.append({"train": {"x": f_train, "y": y_train}, "test": {"x": f_test, "y": y_test}})
    
    if save_folds:
        with open("{}/{}-{}.pkl".format(save_dir, fold_prefix, n_splits), "wb") as fp:
            pickle.dump(folds, fp)    
    return folds

def make_tr_val(file_list, class_name_list, val_per=0.2,save_folds=False, fold_prefix=None, n_splits=1):
    """Creates stratified splits based on train directory listing
    # Arguments
        file_list : A file list of the images you want to split into 
                    a train and validation split. It should include the full
                    relative path with respective to this files current directory
        class_name_list : List of class names as strings
        val_per : Amount of data in validation a number from 0 to 1
        n_splits : number of stratified splits desired (10 for 10-fold split)
        save_folds : Set to True if you want to save the folds created as a pickle file
        fold_prefix : The prefix for the fold file name
    # Dumps
        folds: dict of dicts     {
                                   "train": {
                                               "x": train files list,
                                               "y": train labels},
                                   "val": {
                                               "x": val files list,
                                               "y": val labels}}
                                  }
    """
    files = file_list
    file_labels = np.array([get_folder_above(f) for f in files])
   
    labels = []
    num_classes = len(class_name_list)

    classes = np.r_[0:num_classes]    
    for f in file_labels:
        lb = np.array([f.startswith(x) for x in class_name_list])
        labels.append(classes[lb])
    labels = np.array(labels)
    labels = labels.astype(np.uint16)
#    np.savetxt("labels2.csv", labels)

    # Creating file index list for k-fold cross validation
    folds = {}
    files = np.asarray(files)
#    pprint(files)
    sss = StratifiedShuffleSplit(n_splits=n_splits, test_size=val_per, train_size=1.0-val_per, random_state=None)
    for train_index, val_index in sss.split(files, labels):
        f_train, f_val = files[train_index], files[val_index]
        y_train, y_val = labels[train_index], labels[val_index]
        folds = {"train": {"x": f_train, "y": y_train}, "val": {"x": f_val, "y": y_val}}
        
    if save_folds:
        with open("data/{}-{}.pkl".format(fold_prefix, n_splits), "wb") as fp:
            pickle.dump(folds, fp)    

    return folds

def load_data(in_dir, folds=None, split=None):
    """Builds train/test data from preprocessed features for a given split
    # Arguments
        in_dir: Input directory containing the image files 
        folds: None or list of splits dict{
                                             "train": {
                                                         "x": train files list,
                                                         "y": train labels},
                                             "test": {
                                                         "x": test files list,
                                                         "y": test labels}}
                                        }
        split: None or split number.
    # Returns
        Tran/test data (features and labels) for a given split, if `folds` is not None
        Test data (only features) and file names, if `folds` is None
    """
    if folds:
        y_train = []
        x_train = []
        for f, l in zip(folds[split]["train"]["x"], folds[split]["train"]["y"]):
            x = io.imread.load(f)
            x_train.append(x)
            y_train.append([l])
        x_train = np.vstack(x_train)
        y_train = np.concatenate(y_train)

        y_test = []
        x_test = []
        for f, l in zip(folds[split]["test"]["x"], folds[split]["test"]["y"]):
            x = np.load(f)
            x_test.append(x)
            y_test.append([l])
        x_test = np.vstack(x_test)
        y_test = np.concatenate(y_test)

        return x_train, y_train, x_test, y_test



class kFolded_Dataset(Dataset):
    """K-folded dataset class"""

    def __init__(self, fold_dict, class_names_list,transform=None):
        """
        Args:
            fold_dict (dictionary): A dictionary with the following structrue
                                    {
                                     "x": train/test files list,
                                     "y": train/test labels
                                    }
            
            class_names_list : list of strings which has the class names in it
            transform (callable, optional): Optional transform to be applied
                on a sample.

        """
        self.File_list = fold_dict["x"]
        self.labels = fold_dict["y"]
        self.classes = class_names_list
        self.transform = transform

    def __len__(self):
        return len(self.labels)

    def __getitem__(self, idx):
        img_name = self.File_list[idx]
#        image = io.imread(img_name)        
        image = Image.open(img_name)
#        image = image.transpose(2,0,1)        
        labels = self.labels[idx].astype(np.uint8) 
        labels = np.array([0,1]) if labels == 0 else np.array([1,0])
        sample = {'image': np.asarray(image).transpose(2,0,1), 'labels': labels}
        
        if self.transform:
#            print('image type : {}'.format(type(image)))
#            print('image dtype : {}'.format(image.dtype))
#            print('image shape : {}'.format(image.size))
            
#            image = Image.fromarray(image.transpose(2,0,1))
            sample2 = self.transform(image)
            sample = {'image': sample2, 'labels': labels}
#            print("inside Transform", sample2.shape)
            sample['image'] = np.asarray(sample['image'])
        return sample

class kFolded_Dataset_fastai(Dataset):
    """K-folded dataset class written to comply with my fastai library usage"""

    def __init__(self, fold_dict, class_names_list,transform=None):
        """
        Args:
            fold_dict (dictionary): A dictionary with the following structrue
                                    {
                                     "x": train/test files list,
                                     "y": train/test labels
                                    }
            
            class_names_list : list of strings which has the class names in it
            transform (callable, optional): Optional transform to be applied
                on a sample.

        """
        self.File_list = fold_dict["x"]
        self.labels = fold_dict["y"]
        self.classes = class_names_list
        self.transform = transform
        self.c = len(class_names_list)

    def __len__(self):
        return len(self.labels)

    def __getitem__(self, idx):
        
        img_name = self.File_list[idx]
#        image = io.imread(img_name)        
        image = Image.open(img_name) # This gives HxWxC shape 
#        image = image.transpose(2,0,1)        
#        print('\n\tInside dataloader : ')
#        print('\tPIL image shape : {}'.format(image.size))
        labels = self.labels[idx].astype(np.uint8) 
        labels = np.array([0,1]) if labels == 0 else np.array([1,0])
#        print('\tNumpy image shape : {}'.format((np.asarray(image)).shape))

#        f there are no transforms the following codes output is the sample
        # Convert shape to CxHxW like what pytorch expects and return
        # the sample with image and label
        sample = {'image': np.asarray(image).transpose(2,0,1), 'labels': labels} 
        
        if self.transform:
#            print('image type : {}'.format(type(image)))
#            print('image dtype : {}'.format(image.dtype))
#            print('image shape : {}'.format(image.size))            
#            image = Image.fromarray(image.transpose(2,0,1))

            # in the transforms ToTensor will take care of rearranging 
            # the channel format to what pytorch likes
            sample2 = self.transform(image) 
            sample = {'image': sample2, 'labels': labels}
#            print("inside Transform", sample2.shape)
            sample['image'] = np.asarray(sample['image'])
            labels = np.argmax(sample['labels'])
            sample['labels']  = labels
        return (sample['image'], sample['labels'])


# Ref : http://forums.fast.ai/t/image-normalization-in-pytorch/7534/5
# Implement finding of the mean and std for your dataset
def find_pop_mean_std(train_fold, class_name_list):
    """
    Args:
        train_fold (dictionary): A dictionary with the following structure
                                {
                                     "x": train files list,
                                     "y": train labels
                                }

        
        class_names_list : list of strings which has the class names in it   
        
    """
    # # If dataset can fit in Memory we can load all the images in a 
    # # single batch and calculate the mean and std else split
    # # them up into smaller batches
    # if is_onseshot:

    transform = transforms.Compose([
        transforms.ToTensor()
    ])

    num_train_imgs = len(train_fold["x"])
    print('Number of images in training population : {}'.format(num_train_imgs))
    folded_dataset = kFolded_Dataset(fold_dict=train_fold, 
                                    class_names_list=class_name_list, 
                                    transform=transform)
    dataloader = DataLoader(folded_dataset, 
                             batch_size=num_train_imgs, 
                             shuffle=False, 
                             num_workers=1)

    # Init the population mean and stds
    pop_mean = []
    pop_std0 = []
    pop_std1 = []
    for i, data in enumerate(dataloader):
        print('Processing ....')
        # shape (batch_size, 3, height, width)
        numpy_image = data['image'].numpy()
        print('\tShape of train set : {}'.format(numpy_image.shape))
        # shape (3,)
        batch_mean = np.mean(numpy_image, axis=(0,2,3)) # mean along channel and HxW
        batch_std0 = np.std(numpy_image, axis=(0,2,3)) # std along channel and HxW

        # The following std calc gives a better estimate of the std as the 
        # one above might be an underestimation
        # Ref : https://stackoverflow.com/questions/27600207/why-does-numpy-std-give-a-different-result-to-matlab-std
        batch_std1 = np.std(numpy_image, axis=(0,2,3), ddof=1) 
        
        pop_mean.append(batch_mean)
        pop_std0.append(batch_std0)
        pop_std1.append(batch_std1)
        print('Processing Finished ....')

    # shape (num_iterations, 3) -> (mean across 0th axis) -> shape (3,)
    pop_mean = np.array(pop_mean).mean(axis=0)
    pop_std0 = np.array(pop_std0).mean(axis=0)
    pop_std1 = np.array(pop_std1).mean(axis=0)
    return (pop_mean, pop_std0, pop_std1)


class kFolded_Dataset_withPath(Dataset):
    """K-folded dataset class"""

    def __init__(self, fold_dict, class_names_list,transform=None):
        """
        Args:
            fold_dict (dictionary): A dictionary with the following structrue
                                    {
                                     "x": train/test files list,
                                     "y": train/test labels
                                    }
            
            class_names_list : list of strings which has the class names in it
            transform (callable, optional): Optional transform to be applied
                on a sample.

        """
        self.File_list = fold_dict["x"]
        self.labels = fold_dict["y"]
        self.classes = class_names_list
        self.transform = transform

    def __len__(self):
        return len(self.labels)

    def __getitem__(self, idx):
        img_name = self.File_list[idx]
#        image = io.imread(img_name)        
        image = Image.open(img_name)
#        image = image.transpose(2,0,1)        
        labels = self.labels[idx].astype(np.uint8) 
        labels = np.array([0,1]) if labels == 0 else np.array([1,0])
        sample = {'image': np.asarray(image).transpose(2,0,1), 'labels': labels}
        
        if self.transform:
#            print('image type : {}'.format(type(image)))
#            print('image dtype : {}'.format(image.dtype))
#            print('image shape : {}'.format(image.size))
            
#            image = Image.fromarray(image.transpose(2,0,1))
            sample2 = self.transform(image)
            sample = {'image': sample2, 'labels': labels}
#            print("inside Transform", sample2.shape)
            sample['image'] = np.asarray(sample['image'])
            sample['path'] = img_name
        return sample