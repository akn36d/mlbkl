from __future__ import print_function, division

import torch
import torch.nn as nn
import torch.optim as optim

from torch.optim import lr_scheduler
import numpy as np
import torchvision 
from torchsummary import summary
from torchvision import datasets, models, transforms
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import time
import os
import copy

from models import *
from utils import *
from data import *

def transfer_resnet():
    # PRELIM SETUP
    models_dir = 'models'
    data_dir = 'mel_bkl'
    class_names = ["mel","bkl"]
    
    
    # LOAD DATA
    # Data augmentation and normalization for training
    # Just normalization for validation
    data_transforms = {
        'train': transforms.Compose([
            transforms.RandomResizedCrop(224),
            transforms.RandomHorizontalFlip(),
            transforms.ToTensor(),
            ]),
        'val': transforms.Compose([
            transforms.Resize(256),
            transforms.CenterCrop(224),
            transforms.ToTensor(),
            ])
    }
    
    
    # Generate the folds for a 10-fold cross validation
    fold10_cv = make_folds(data_dir, class_names, file_template='*.jpg', n_splits=10, save_folds=True, fold_prefix='test_folding',save_dir='fold')
    # pprint(fold10_cv)
    print(2*"\n")
    for fold_num, fold in enumerate(fold10_cv):
        print(80*"=")
        print("FOLD - {}".format(fold_num))
        print(80*"=")
        # pprint(fold)
    
        train_files = fold["train"] 
        test_files = fold["test"]
        
        print('Number of test images in fold: ', len(train_files["x"]))
        print('Number of test images in fold: ', len(test_files["x"]))

        train_val_split = make_tr_val(file_list=train_files["x"], class_name_list=class_names, val_per=0.2)
        
    
        # x_train = train_val_split["train"]
    
        print('Number of training images: ',len(train_val_split['train']['x']))
        print('Number of val images: ', len(train_val_split['val']['x']))
        
    
        # y_train = train_val_split["train"]["y"]
    
        # x_val = train_val_split["val"]["x"]
        # y_val = train_val_split["val"]["y"]
    
        # Do dictionary comprehension to process data 
        image_dataset = {x: kFolded_Dataset(fold_dict=train_val_split[x], class_names_list=class_names, transform=data_transforms[x])
                                           for x in ['train', 'val']}   
        # print(image_dataset['train'].File_list[0])
        
        dataloaders = {x: torch.utils.data.DataLoader(image_dataset[x], batch_size=6, shuffle=True, num_workers=1) for x in ['train', 'val']}
    
        dataset_sizes = {x: len(image_dataset[x]) for x in ['train', 'val']}
        class_names = image_dataset['train'].classes
    
        device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    
    #    tensor_imshow(dataloaders=dataloaders, class_names=class_names, phase='train')
    #    plt.show()
            
        # Finetuning a pre-trained model
        model_ft = models.resnet18(pretrained=True)
        
        # Freezing all layers except last fully connected layer
        for param in model_ft.parameters():
            param.requires_grad = False
       
        num_ftrs = model_ft.fc.in_features
        model_ft.fc = nn.Sequential(nn.Linear(num_ftrs, 2), nn.Softmax())
        
        
    
        model_ft = model_ft.to(device)
        
        # Looking at the network layers
        # pprint(model_ft)
        # print_model_keys(model_ft)
        # print_model_child(model_ft)
       # summary(model_ft, (3, 224, 224))
        
        criterion = nn.CrossEntropyLoss()
    
        # Observe that all params are being optimized
        optimizer_ft = optim.SGD(model_ft.parameters(), lr=0.001, momentum=0.9)
    
        # Decay LR by a factor of 0.1 every 7 epochs
        exp_lr_scheduler = lr_scheduler.StepLR(optimizer_ft, step_size=7, gamma=0.1)
    
    
        # Train and Evaluate 
        model_ft = train_model( dataloaders=dataloaders, dataset_sizes=dataset_sizes,
                                model=model_ft, 
                                criterion=criterion, 
                                optimizer=optimizer_ft, 
                                scheduler=exp_lr_scheduler, 
                                device=device, num_epochs=25)
    
        # saving the model on the k-th fold
        model_fname = os.path.join(models_dir,"model_fld{}.pt".format(fold_num))
        torch.save(model_ft,model_fname)
        
    
# Safe DataLoader multiprocessing with Windows
if __name__ == '__main__':
    transfer_resnet()
    