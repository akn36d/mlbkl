from __future__ import print_function, division

import torch
import torch.nn as nn
import torch.optim as optim

from fastai.imports import * # Probably imports tvm i.e. the torchvision models
from fastai.vision.learner import ConvLearner
from fastai.data import DataBunch
from fastai.metrics import accuracy
from fastai.train import ShowGraph

import numpy as np
import torchvision 
from torchsummary import summary
from torchvision import datasets, models, transforms

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

import pickle
import time
import os
import copy

# Import to stop displaying a progressbar
from fastprogress import force_console_behavior
import fastprogress
fastprogress.fastprogress.NO_BAR = False
# master_bar, progress_bar = force_console_behavior()

from models import *
from utils import *
from data import *

# LOAD DATA
models_dir = 'models'
data_dir = 'mel_bkl'
class_names = ["mel","bkl"]

# Data augmentation and normalization for training
# Just normalization for validation
data_transforms = {
    'train': transforms.Compose([
        transforms.ToTensor(),
        ]),
    'val': transforms.Compose([
        transforms.ToTensor(),
        ])
}


# Generate the folds for a 10-fold cross validation
fold10_cv = make_folds(data_dir, class_names, file_template='*.jpg', n_splits=10, save_folds=True, fold_prefix='test_folding',save_dir='fold')
# pprint(fold10_cv)
print(2*"\n")
for fold_num, fold in enumerate(fold10_cv):
    print(80*"=")
    print("FOLD - {}".format(fold_num))
    print(80*"=")
    # pprint(fold)

    train_files = fold["train"] 
    test_files = fold["test"]
    
    print('Number of test images in fold: ', len(train_files["x"]))
    print('Number of test images in fold: ', len(test_files["x"]))

    train_val_split = make_tr_val(file_list=train_files["x"], class_name_list=class_names, val_per=0.2)
    
    print('Number of training images: ',len(train_val_split['train']['x']))
    print('Number of val images: ', len(train_val_split['val']['x']))
    
    image_dataset = kFolded_Dataset_fastai(fold_dict=train_val_split['train'], class_names_list=class_names, transform=data_transforms['train'])
    
    dataloader = torch.utils.data.DataLoader(image_dataset, batch_size=1, shuffle=True, num_workers=1) 

    image, sample = next(iter(dataloader))
    
    print('\nImage type : {}'.format(image))
    print('Image shape : {} \n\n'.format(image.size()))
    print(image)
    
    break