from __future__ import print_function, division

import torch
import numpy as np

import os

from utils import *
from data import *

# LOAD DATA info
models_dir = 'models'
data_dir = 'mel_bkl'
class_names = ["mel","bkl"]


# Generate the folds for a 10-fold cross validation
fold10_cv = make_folds(data_dir, class_names, file_template='*.jpg', n_splits=10, save_folds=True, fold_prefix='test_folding',save_dir='fold')
# pprint(fold10_cv)
print(2*"\n")
for fold_num, fold in enumerate(fold10_cv):
    print(80*"=")
    print("FOLD - {}".format(fold_num))
    print(80*"=")

    train_files = fold["train"] 
    test_files = fold["test"]
    
    print('Number of test images in fold: ', len(train_files["x"]))
    print('Number of test images in fold: ', len(test_files["x"]))

    train_val_split = make_tr_val(file_list=train_files["x"], class_name_list=class_names, val_per=0.2)
    
    print('Number of training images: ',len(train_val_split['train']['x']))
    print('Number of val images: ', len(train_val_split['val']['x']))
    
    (pop_mean, pop_std0, pop_std1) = find_pop_mean_std(train_fold=train_val_split['train'], class_name_list=class_names)
    print('\nTrain set channel means : {}'.format(pop_mean))
    print('Train set channel std0 : {}'.format(pop_std0))
    print('Train set channel std1 : {}'.format(pop_std1))
    
    break