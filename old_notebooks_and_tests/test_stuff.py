from utils import recursive_glob
from pprint import pprint
from data import make_folds


root_dir = 'mel_bkl'

cl_list = ['mel', 'bkl']
n_splits = 10
fold10_cv = make_folds(data_dir=root_dir, class_name_list=cl_list, file_template='*.jpg', n_splits=n_splits)
# pprint(fold10_cv)