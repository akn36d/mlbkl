from __future__ import print_function, division

import torch
import torch.nn as nn
import torch.optim as optim

from fastai.basic_train import Learner
from fastai.vision.learner import ConvLearner
from fastai.data import DataBunch
from fastai.metrics import accuracy
from fastai.train import ShowGraph

import numpy as np
import torchvision 
from torchsummary import summary
from torchvision import datasets, models, transforms

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

import pickle
import time
import os
import copy

# Import to stop displaying a progressbar
from fastprogress import force_console_behavior
import fastprogress
fastprogress.fastprogress.NO_BAR = False
# master_bar, progress_bar = force_console_behavior()

from models import *
from utils import *
from data import *

def transfer_resnet():
    # PRELIM SETUP
    models_dir = 'models'
    data_dir = 'mel_bkl'
    class_names = ["mel","bkl"]
    
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

    # LOAD DATA
    # Data augmentation and normalization for training
    # Just normalization for validation
    data_transforms = {
        'train': transforms.Compose([
            transforms.RandomResizedCrop(224),
            transforms.RandomHorizontalFlip(),
            transforms.ToTensor(),
            ]),
        'val': transforms.Compose([
            transforms.Resize(256),
            transforms.CenterCrop(224),
            transforms.ToTensor(),
            ])
    }
    
    
    # Generate the folds for a 10-fold cross validation
    fold10_cv = make_folds(data_dir, class_names, file_template='*.jpg', n_splits=10, save_folds=True, fold_prefix='test_folding',save_dir='fold')
    # pprint(fold10_cv)
    print(2*"\n")
    for fold_num, fold in enumerate(fold10_cv):
        print(80*"=")
        print("FOLD - {}".format(fold_num))
        print(80*"=")
        # pprint(fold)
    
        train_files = fold["train"] 
        test_files = fold["test"]

        # Creating a dataloader for the test data
        test_dataset = kFolded_Dataset(fold_dict=test_files, class_names_list=class_names, transform=data_transforms['val'])
        test_dataloader = torch.utils.data.DataLoader(test_dataset, batch_size=6, shuffle=True, num_workers=1)
        
        print('Number of test images in fold: ', len(train_files["x"]))
        print('Number of test images in fold: ', len(test_files["x"]))

        train_val_split = make_tr_val(file_list=train_files["x"], class_name_list=class_names, val_per=0.2)
        
        # # reducing training set size for faster debug
        # train_val_split['train']['x'] = train_val_split['train']['x'][0:100,...]
        # train_val_split['train']['y'] = train_val_split['train']['y'][0:100,...] 
    
        # x_train = train_val_split["train"]
    
        print('Number of training images: ',len(train_val_split['train']['x']))
        print('Number of val images: ', len(train_val_split['val']['x']))
        
    
        # y_train = train_val_split["train"]["y"]
    
        # x_val = train_val_split["val"]["x"]
        # y_val = train_val_split["val"]["y"]
    
        # Do dictionary comprehension to process data 
        image_dataset = {x: kFolded_Dataset_fastai(fold_dict=train_val_split[x], class_names_list=class_names, transform=data_transforms[x])
                                           for x in ['train', 'val']}   
        # print(image_dataset['train'].File_list[0])
        
        dataloaders = {x: torch.utils.data.DataLoader(image_dataset[x], batch_size=6, shuffle=True, num_workers=1) 
                                                     for x in ['train', 'val']}
        
        # creating fastai databunches from the pytorch dataloaders
        data_bunches = DataBunch(train_dl=dataloaders['train'], 
                                 valid_dl=dataloaders['val'], 
                                 test_dl=test_dataloader,
                                 device=device)

        dataset_sizes = {x: len(image_dataset[x]) for x in ['train', 'val']}
        class_names = image_dataset['train'].classes
    
        
    
        # tensor_imshow(dataloaders=dataloaders, class_names=class_names, phase='train')
        # plt.show()
            
        # Finetuning a pre-trained model
        model_ft = models.resnet18(pretrained=True)
        
        # Freezing all layers except last fully connected layer
        for param in model_ft.parameters():
            param.requires_grad = False
       
        num_ftrs = model_ft.fc.in_features
        model_ft.fc = nn.Sequential(nn.Linear(num_ftrs, 2), nn.Softmax(dim=0))

        # Setting optimizer and loss
        optmzr = optim.Adam(model_ft.parameters())
        criterion = nn.CrossEntropyLoss()

        # Using the fastai learner class to fit my model
        fig = plt.figure()
        learn = Learner(data=data_bunches, model=model_ft, 
                        metrics=accuracy,
                        model_dir=models_dir,
                        callback_fns=ShowGraph)
        plt.xlabel('epochs')
        plt.ylabel('Loss')
        plt.savefig('tr_val_loss_plot.png', dpi=fig.dpi)

        # Trying to find optimum base_lr to max_lr range
        lr_range_find = False
        if lr_range_find:
            learn.lr_find(start_lr=1e-4,end_lr=10,num_it=267) # From image we can see (0.001 to 0.1) is a good range
                                                              # for cyclical learning
            fig = plt.figure()
            learn.recorder.plot()
            plt.xlabel('Learning rate')
            plt.ylabel('Validation Loss')
            plt.savefig('scheduler_plot.png', dpi=fig.dpi)
        

        # fit the model using the cyclic scheduler
        learn.fit_one_cycle(cyc_len=10, max_lr=0.0001*99)

        # Plot the important stuff
        fig = plt.figure()    
        learn.recorder.plot_losses()
        plt.legend('training','validation')
        plt.title('Loss vs Num_its')
        plt.savefig(os.path.join(models_dir,'model_fastai_fld_losses{}.png'.format(fold_num)), dpi=fig.dpi)

        fig = plt.figure() 
        learn.recorder.plot_lr(show_moms = False)
        plt.title('Learning Reate vs Num_its')
        plt.savefig(os.path.join(models_dir,'model_fastai_fld_lr{}.png'.format(fold_num)), dpi=fig.dpi)

        fig = plt.figure() 
        learn.recorder.plot_metrics()
        plt.title('Accuracy vs Num_its')
        plt.savefig(os.path.join(models_dir,'model_fastai_fld_metrics{}.png'.format(fold_num)), dpi=fig.dpi)

        # saving the model on the k-th fold
        model_fname = os.path.join("model_fastai_fld{}".format(fold_num))
        learn.save(model_fname)
        
        # Pickle the metrics and loss vals
        with open(os.path.join(models_dir,'stats_fld{}.pkl'.format(fold_num)), 'w') as f:
            pickle.dump([learn.recorder.lrs,
                         learn.recorder.losses,
                         learn.recorder.val_loss,
                         learn.recorder.metrics],f)
    
# Safe DataLoader multiprocessing with Windows
if __name__ == '__main__':
    transfer_resnet()
    