from __future__ import print_function, division

import torch
import torch.nn as nn
import torch.optim as optim

from torch.optim import lr_scheduler
import numpy as np
import torchvision 
from torchvision import datasets, models, transforms, utils
import matplotlib.pyplot as plt

import collections
import time
import sys
import os
import copy
plt.ioff()

def makefolder_ifnotexists(foldername):
    if not os.path.exists(foldername):
        os.makedirs(foldername)

# Set up all the required folders for your model
def run_environ_setup(model_name, run_num, train_num):
    '''
    Make sure the utilities.py folder is in the same folder as the training code 
    Sets up the folders as follows

    Models_runs
    |--Suite_name
       |--logs
       |--weights
       |--config
    '''
    # get directory script resides in
    # Suite Name and directory declaration (The place where all the info for a given run 
    # of a model will be stored)
    models_folder = 'Model_runs'
    makefolder_ifnotexists(os.path.join(models_folder))

    # Specify Model name to save model run information in Model_runs folder
    suite_dirname = model_name + '_' + run_num
    # suite_dirname = input('\nEnter name for this run of model: ')
    suite_fname = os.path.join(models_folder, suite_dirname)
    makefolder_ifnotexists(os.path.join(models_folder, 
                                        suite_dirname))

    # log folder for run of model
    log_path = 'logs'
    makefolder_ifnotexists(os.path.join(models_folder,
                                        suite_dirname, 
                                        log_path))
    log_dir_path = os.path.join(models_folder,
                                suite_dirname, 
                                log_path)

    # model folder for saving weights in a run of model
    save_weights_path = 'weights'
    makefolder_ifnotexists(os.path.join(models_folder,
                                        suite_dirname ,
                                        save_weights_path))
    save_weights_path = os.path.join(suite_dirname ,
                                     save_weights_path)
    

    # settings/config folder for a run of model
    config_path = 'config'
    makefolder_ifnotexists(os.path.join(models_folder,
                                        suite_dirname ,
                                        config_path))
    config_path = os.path.join(models_folder,
                               suite_dirname,
                               config_path)
    
    return suite_fname, models_folder, log_dir_path, save_weights_path, config_path



def tensor_imshow(dataloaders, class_names, phase='train'):
    """Imshow for Tensor."""
    # Get a batch of training data
    data_batch = next(iter(dataloaders[phase]))

    if isinstance(data_batch, collections.Mapping):
        inputs = data_dict['image']
        classes = data_dict['labels']
    else:
        inputs = data_batch[0]
        classes = data_batch[1]
    
    # Make a grid from batch
    out = torchvision.utils.make_grid(inputs.data, normalize=True)
    
    # deprocessing the images
    inp = out.numpy().transpose((1, 2, 0))

    fig = plt.figure()
    plt.imshow(inp)

    title=[class_names[x] for x in classes]
    plt.title(title)   

    plt.savefig('data_batch.png', dpi=fig.dpi)


def recursive_glob(root_dir, file_template="*.tif"):
    """Traverse directory recursively. Starting with Python version 3.5, the glob module supports the "**" directive"""

    if sys.version_info[0] * 10 + sys.version_info[1] < 35:
        import fnmatch
        import os
        matches = []
        for root, dirnames, filenames in os.walk(root_dir):
            for filename in fnmatch.filter(filenames, file_template):
                matches.append(os.path.join(root, filename))
        return matches
    else:
        import glob
        file_list = glob.glob(root_dir + "/**/" + file_template, recursive=True)
        # print(file_list)
        return file_list

def get_folder_above(fpath):
    path_part1, _ = os.path.split(fpath)
    _, folder_above = os.path.split(path_part1)
    return folder_above


# Ref : https://github.com/fastai/fastai/commit/2c41d6321916936869ea61c4978a294c54fc3b09
def range_of(x): return list(range(len(x)))
def arange_of(x): return np.arange(len(x))
def trange_of(x): return torch.arange(len(x))


# for all below Ref : https://github.com/fastai/fastai/blob/master/fastai/basic_train.py#L189
def plot_lr(lrs, moms, show_moms=False, save_fig=False, save_path=None)->None:
    "Plot learning rate, `show_moms` to include momentum."
    lrs = np.asarray(lrs).flatten() 
    moms = np.asarray(moms).flatten()
    iterations = arange_of(lrs)

    fig = plt.figure()       
    if show_moms:
        _, axs = plt.subplots(1,2, figsize=(12,4))
        axs[0].plot(iterations, lrs)
        axs[0].set_title('Loss vs Num_its')
        axs[1].plot(iterations, moms)
        axs[1].set_title('Moms vs Num_its')
    else: 
        plt.plot(iterations, lrs)
        plt.title('Loss vs Num_its')
    if save_fig:        
        plt.savefig(save_path, dpi=fig.dpi)
    return lrs, moms


def plot_lr_loss(lrs, losses, skip_start:int=10, skip_end:int=5, save_fig=False, save_path=None)->None:
    "Plot learning rate and losses, trimmed between `skip_start` and `skip_end`."
    fig = plt.figure()   
    lrs = lrs[skip_start:-skip_end] if skip_end > 0 else self.lrs[skip_start:]
    losses = losses[skip_start:-skip_end] if skip_end > 0 else losses[skip_start:]
    _, ax = plt.subplots(1,1)
    ax.plot(lrs, losses)
    ax.set_xscale('log')
    plt.title('Loss vs lr')
    if save_fig:        
        plt.savefig(save_path, dpi=fig.dpi)


def plot_losses(losses, val_losses, nb_epochs, nb_batches_list, save_fig=False, save_path=None)->None:
    "Plot training and validation losses."

    losses = np.asarray(losses).reshape(-1,)
    val_losses = np.asarray(val_losses).reshape(-1,)
    iterations = arange_of(losses)
    
    fig = plt.figure()    
    _, ax = plt.subplots(1,1)
    ax.plot(iterations, losses)
    
    val_iter = np.cumsum(np.asarray(nb_batches_list).reshape(-1,))        
#     print('val_iter : {}'.format(val_iter))
#     print('val_losses shape : {}'.format(val_losses))
    ax.plot(val_iter, val_losses, 'k', ls='--')
    ax.legend(['training','validation'])
    ax.set_title('Loss vs Num_its')
    if save_fig:        
        plt.savefig(save_path, dpi=fig.dpi)
    return losses, val_losses


def plot_metrics(metrics, nb_epochs, save_fig=False, save_path=None)->None:
    "Plot metrics collected during training."
    metrics = np.asarray(metrics).flatten().reshape(1,-1)
    fig = plt.figure()        
    assert len(metrics) != 0, "There are no metrics to plot."
    _, ax = plt.subplots(1,1)
    ax.plot(np.arange(nb_epochs).reshape(1,-1), metrics, 'ro', ls='--')
    ax.set_title('Metrics vs Num_its')
    if save_fig:        
        plt.savefig(save_path, dpi=fig.dpi)
    return metrics
        
