from __future__ import print_function, division

import torch
import torch.nn as nn
import torch.optim as optim

from torch.optim import lr_scheduler
import numpy as np
import torchvision 
from torchvision import datasets, models, transforms
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import time
import os
import copy

from pprint import pprint

from utils import *


def train_model( dataloaders, dataset_sizes, model, criterion, optimizer, scheduler, device, num_epochs=25):
    since = time.time()

    #  state_dict() - Returns a dictionary containing a whole state of the module 
    # (Below we init it to store the best weights later on).
    # Both parameters and persistent buffers (e.g. running averages) are included. 
    # Keys are corresponding parameter and buffer names.
    # Returns:    a dictionary containing a whole state of the module
    # Return type:    dict
    best_model_wts = copy.deepcopy(model.state_dict())

    best_acc = 0.0

    for epoch in range(num_epochs):
        print('Epoch {}/{}'.format(epoch, num_epochs - 1))
        print('-' * 10)

        # Each epoch has a training and validation phase
        for phase in ['train', 'val']:
            if phase == 'train':
                scheduler.step() # Get a value for the lr (training schedule)
                model.train() # Sets model in training mode (this effectively disables some layer like )
            else:
                model.eval()

            running_loss = 0.0
            running_corrects = 0 

            # iterate over data
            for data_dict in dataloaders[phase]:
                data_dict = next(iter(dataloaders[phase]))

                inputs = data_dict['image'].float()
                labels = data_dict['labels'].long()
#                print('inside model ',labels.size(), labels.dtype)
                inputs = inputs.to(device)
                labels = labels.to(device)

                # Zero param grads
                optimizer.zero_grad()

                # FORWARD:
                # track history if only in train
                with torch.set_grad_enabled(phase == 'train'): # set_grad_enabled - Context-manager that sets gradient calculation to on or off.
                    outputs = model(inputs) # Doing the prediction in forward pass
#                    print('outputs', outputs.size(), outputs.dtype)
#                    print(outputs)
                        
                    # Returns the maximum value of each row of the input tensor in the given dimension dim. 
                    # The second return value is the index location of each maximum value found (argmax).
                    # print(outputs.size())
                    _, labels = torch.max(labels, dim=1)
                    _, preds = torch.max(input=outputs, dim=1) # 
#                    print('prop ',prop)
#                    print('prediction', preds.size(), preds.dtype)
#                    print(preds)
#                    print(labels)
                    loss = criterion(outputs, labels.long())

                    # backward + optimize only if in training phase 
                    if phase == 'train':
                        loss.backward() # calculate gradients with back pass
                        optimizer.step() # Do an update of the params based onn backpass  

                # statistics
                running_loss += loss.item() *  inputs.size(0) # -> average loss on batch * batch_size = total loss for batch
                running_corrects += torch.sum(preds == labels.data) # number of correct predictions

            epoch_loss = running_loss / dataset_sizes[phase]
            epoch_acc = running_corrects.double() / dataset_sizes[phase]

            print('{} Loss: {:.4f}, Acc: {:.4f} '.format(phase, epoch_loss, epoch_acc))
            
            # deep copy model if improvement
            if phase == 'val' and epoch_acc > best_acc:
                best_acc = epoch_acc
                best_model_wts = copy.deepcopy(model.state_dict())
        
        print()

    time_elapsed = time.time() - since
    print('Training complete in {:.0f}m {:.0f}s'.format(time_elapsed//60, time_elapsed%60))

    print('Best val Acc: {:4f}'.format(best_acc))

    # Load best model weights
    model.load_state_dict(best_model_wts)
    return model


def print_model_keys(model):
    pprint(model.keys())



def visualize_model(dataloaders, model, num_images=6):
    # check to see in the model has been trained and save that state
    # before we call eval() to do prediction for visualization.
    was_training = model.training # The "training" variable in torchs "module" class is
                                  # only set true if the "train()" was called on the 
                                  # model
    model.eval()
    images_so_far = 0

    fig = plt.figure()

    with torch.no_grad():
        for i, (inputs, labels) in enumerate(dataloaders['val']):
            inputs = inputs.to(device)
            labels = inputs.to(device)

            outputs = model(inputs)
            _, preds = torch.max(outputs, 1)

            for j in range(inputs.size()[0]):
                images_so_far += 1
                ax = plt.subplot(num_images//2, 2, images_so_far)
                ax.axis('off')
                ax.set_title('predicted: {}'.format(class_names[preds[j]]))
                imshow(inputs.cpu().data[j])

                if images_so_far == num_images:
                    plt.savefig('pred_viz.png', dpi=fig.dpi)
                    model.train(mode=was_training) # Setting "training" var to prior state before 
                                                   # the visualization
                    return
        model.train(mode=was_training)  # Setting "training" var to prior state before 
                                                   # the visualization     

def print_model_child(model):
    child_counter = 0
    for child in model.children():
        print("    child", child_counter, " is -")
        pprint(child)
        child_counter += 1    