from __future__ import print_function, division

import torch
import torch.nn as nn
import torch.optim as optim

from torch.optim import lr_scheduler
import numpy as np
import torchvision 
from torchsummary import summary
from torchvision import datasets, models, transforms
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import time
import os
import copy

from models import *
from utils import *
from data import *

# PRELIM SETUP
models_dir = 'models'
data_dir = 'mel_bkl'
class_names = ["mel","bkl"]


# LOAD DATA
# Data augmentation and normalization for training
# Just normalization for validation
data_transforms = {
    'train': transforms.Compose([
        transforms.RandomResizedCrop(224),
        transforms.RandomHorizontalFlip(),
        transforms.ToTensor(),
        transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
        ]),
    'val': transforms.Compose([
        transforms.Resize(256),
        transforms.CenterCrop(224),
        transforms.ToTensor(),
        transforms.Normalize([0.485,0.456,0.405], [0.229, 0.224, 0.225])
        ])
}


# Generate the folds for a 10-fold cross validation
fold10_cv = make_folds(data_dir, class_names, file_template='*.jpg', save_folds=False)
# pprint(fold10_cv)
print(2*"\n")
for fold_num, fold in enumerate(fold10_cv):
    print(80*"=")
    print("FOLD - {}".format(fold_num))
    print(80*"=")
    # pprint(fold)

    train_files = fold["train"] 
    test_files = fold["test"]

    train_val_split = make_tr_val(file_list=test_files["x"], class_name_list=class_names, val_per=0.2)
    

    # x_train = train_val_split["train"]

    print('Number of training images: ',len(train_val_split['train']['x']))
    print('Number of val images: ', len(train_val_split['val']['x']))
    print('Number of test images: ', len(test_files["x"]))

    # y_train = train_val_split["train"]["y"]

    # x_val = train_val_split["val"]["x"]
    # y_val = train_val_split["val"]["y"]

    # Do dictionary comprehension to process data 
    image_dataset = {x: kFolded_Dataset(fold_dict=train_val_split[x], class_names_list=class_names, transform=data_transforms[x])
                                       for x in ['train', 'val']}   
    # print(image_dataset['train'].File_list[0])
    
    dataloaders = {x: torch.utils.data.DataLoader(image_dataset[x], batch_size=4, shuffle=True, num_workers=4) for x in ['train', 'val']}

    dataset_sizes = {x: len(image_dataset[x]) for x in ['train', 'val']}
    class_names = image_dataset['train'].classes

    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

    tensor_imshow(dataloaders=dataloaders, class_names=class_names, phase='train')
    quit()
    
    # Finetuning a pre-trained model
    model_ft = models.resnet18(pretrained=True)

    num_ftrs = model_ft.fc.in_features
    model_ft.fc = nn.Linear(num_ftrs, 2)

    model_ft = model_ft.to(device)

    # Looking at the network layers
    # pprint(model_ft)
    # print_model_keys(model_ft)
    # print_model_child(model_ft)
    # summary(model_ft, (3, 224, 224))

    criterion = nn.CrossEntropyLoss()

    # Observe that all params are being optimized
    optimizer_ft = optim.SGD(model_ft.parameters(), lr=0.001, momentum=0.9)

    # Decay LR by a factor of 0.1 every 7 epochs
    exp_lr_scheduler = lr_scheduler.StepLR(optimizer_ft, step_size=7, gamma=0.1)


    # Train and Evaluate 
    model_ft = train_model( dataloaders=dataloaders, dataset_sizes=dataset_sizes,
                            model=model_ft, 
                            criterion=criterion, 
                            optimizer=optimizer_ft, 
                            scheduler=exp_lr_scheduler, 
                            device=device, num_epochs=25)

    # saving the model on the k-th fold
    model_fname = os.path.join(models_dir,"model_fld{}.pt")
    torch.save(model_ft,model_fname)

'''
'''
